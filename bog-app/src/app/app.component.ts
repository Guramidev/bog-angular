import { Component } from '@angular/core';
import { ModalService } from './shared/components/modal/modal.service';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { of } from 'rxjs';
import { delay } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

}
