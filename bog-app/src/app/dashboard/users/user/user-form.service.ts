import { Injectable } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { UsersService } from '../users.service';
import { map } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable()
export class UserFormService {
  form = this.formBuilder.group({
    id: '',
    firstName: ['', Validators.minLength(2)],
    lastName: ['', Validators.minLength(2)],
    age: ['', Validators.min(10)],
    email: ['', [Validators.email], (c: FormControl) => {
      if (!c.parent || c.parent.get('existingEmail').value && c.value ===
        c.parent.get('existingEmail').value) {


        return of(null);
      }

      return this.usersService.getUsers()
        .pipe(
          map((users: any[]) => {
            return users.filter(u => u.email === c.value);
          }),
          map((users) => {
            return users.length ? {emailExists: 'Email exists'} : null;
          })
        );
    }],
    confirmEmail: ['', (c: FormControl) => {
      if (c.parent && c.value !== c.parent.get('email').value) {
        return {
          emailMatch: 'Mismatch'
        };
      }
      return null;
    }],
    existingEmail: '',
    password: ['', Validators.minLength(3)],
    confirmPassword: ['', (c: FormControl) => {
      if (c.parent && c.value !== c.parent.get('password').value) {
        return {
          passwordMatch: 'Mismatch'
        };
      }
    }]
  });

  constructor(
    private formBuilder: FormBuilder,
    private usersService: UsersService
  ) { }
}
