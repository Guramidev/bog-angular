import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserFormService } from './user-form.service';
import { UsersService } from '../users.service';
import { UserService } from './user.service';
import { IUser } from './user-model';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss'],
  providers: [UserFormService, UsersService, UserService]
})
export class UserComponent implements OnInit {

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    public userFormService: UserFormService,
    private userService: UserService
  ) { }

  ngOnInit() {
    this.getUser();
  }

  onSubmit() {
    if (this.userFormService.form.valid) {
      const {firstName, lastName, age, password, email} =
        this.userFormService.form.value;
      this.userService.postUser(
        {firstName, lastName, age, password, email}
      ).toPromise().then((res: IUser) => {
        if (res.id) {
          this.router.navigate([res.id], {
            relativeTo: this.activatedRoute
          });
        }
      });
    } else {
      console.log(this.userFormService.form);
      Object.values(this.userFormService.form.controls)
        .forEach(c => {
          c.markAsDirty();
        });
    }
  }

  private getUser() {
    const id = this.activatedRoute.snapshot.paramMap.get('id');

    if (id) {
      this.userService.getUser(id).toPromise()
        .then((user) => {
          this.userFormService
            .form.get('confirmPassword').setValue(user.password);
          this.userFormService
            .form.get('confirmEmail').setValue(user.email);
          this.userFormService
            .form.get('existingEmail').setValue(user.email);
          this.userFormService.form.patchValue(user);
        });
    }
  }
}
