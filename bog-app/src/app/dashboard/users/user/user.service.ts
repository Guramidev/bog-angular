import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IUser } from './user-model';

@Injectable()
export class UserService {

  constructor(
    private httpClient: HttpClient
  ) { }

  getUser(id: string) {
    return this.httpClient
      .get<IUser>('http://localhost:3000/users/' + id);
  }

  postUser(user: IUser) {
    return this.httpClient.post('http://localhost:3000/users', user);
  }
}
