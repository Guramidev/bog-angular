import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'usersSearch'
})
export class UsersSearchPipe implements PipeTransform {

  transform(users: any = [], searchString: string = ''): any {
    console.log(users, searchString);
    return users.filter(v => v.firstName.includes(searchString));
  }

}
