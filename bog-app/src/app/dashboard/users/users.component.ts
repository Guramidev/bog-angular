import { Component, OnInit } from '@angular/core';
import { UsersService } from './users.service';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-users',
  providers: [UsersService],
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  searchString = new FormControl();
  users$: Observable<any[]> = this.searchString.valueChanges
    .pipe(
      startWith(''),
      switchMap((searchString) => {
        return this.usersService.getUsers()
          .pipe(
            map((users: any[]) => {
              return users
                .filter(v => v.firstName.includes(searchString));
            })
          );
      })
    );

  constructor(
    private usersService: UsersService
  ) {}


  ngOnInit() {
    // this.usersService.getUsers()
    //   .toPromise()
    //   .then((users) => {
    //     this.users = users;
    //   });
    this.searchString.valueChanges.subscribe(console.log);
    // console.log(
    //
    // );
  }
}
