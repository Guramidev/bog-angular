import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule } from './users-routing.module';
import { UsersComponent } from './users.component';
import { ComponentsModule } from '../../shared/components/components.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UsersSearchPipe } from './users-search.pipe';
import { UserComponent } from './user/user.component';

@NgModule({
  imports: [
    CommonModule,
    UsersRoutingModule,
    ComponentsModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  declarations: [
    UsersComponent,
    UsersSearchPipe,
    UserComponent
  ]
})
export class UsersModule { }
