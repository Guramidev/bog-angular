import { Injectable } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class LoginFormService {
  constructor(
    private formBuilder: FormBuilder
  ) { }

  getSignInForm() {
    return this.formBuilder.group({
      email: [null, Validators.email],
      password: [null, Validators.minLength(3)]
    });
  }
}
