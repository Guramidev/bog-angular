import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ModalService } from '../shared/components/modal/modal.service';
import { ForgotPasswordComponent } from '../forgot-password/forgot-password.component';
import { LoginService } from './login.service';
import { Router } from '@angular/router';
import { LoginFormService } from './login-form.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  providers: [LoginService, LoginFormService],
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  signInForm = this.loginFormService.getSignInForm();

  constructor(
    private modalService: ModalService,
    private formBuilder: FormBuilder,
    private loginService: LoginService,
    private router: Router,
    private loginFormService: LoginFormService
  ) {}

  onForgotPasswordClick(e) {
    e.preventDefault();
    this.modalService.open(ForgotPasswordComponent);
  }

  onSubmit() {
    if (this.signInForm.dirty && this.signInForm.valid) {
      const {email, password} = this.signInForm.value;
      this.loginService.signIn(
        email,
        password
      ).subscribe((user) => {
        if (user) {
          localStorage.setItem('isAuthenticated', '1');
          this.router.navigate(['dashboard']);
        } else {
          alert('yes1');
        }
      }, (err) => {
        console.log(err);
      });
    } else {
      console.log(this);
    }
  }
}
