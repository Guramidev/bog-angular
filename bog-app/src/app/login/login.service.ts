import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IUser } from './login-model';
import { catchError, map, tap } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(
    private httpClient: HttpClient
  ) { }

  getUsers() {
    return this.httpClient.get<IUser[]>('http://localhost:3000/users');
  }

  signIn(email: string, password: string) {
    return this.getUsers().pipe(
      map((users) =>
        users.filter(user =>
          user.email === email && user.password === password)[0]),
      catchError((err) => throwError('Server unresponsive'))
    );
  }
}
