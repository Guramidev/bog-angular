import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavComponent } from './nav/nav.component';
import { NavItemComponent } from './nav/nav-item/nav-item.component';
import { NavLabelDirective } from './nav/nav-label/nav-label.directive';
import { ButtonComponent } from './button/button.component';
import { RippleComponent } from './ripple/ripple.component';
import { ModalComponent } from './modal/modal.component';
import { InputComponent } from './input/input.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  declarations: [NavComponent, NavItemComponent, NavLabelDirective,
    ButtonComponent,
    RippleComponent,
    ModalComponent,
    InputComponent],
  exports: [NavComponent, NavItemComponent, NavLabelDirective,
    ButtonComponent, RippleComponent,
    InputComponent],
  entryComponents: [
    ModalComponent
  ]
})
export class ComponentsModule { }
