import { Component, Input, OnInit, InjectionToken, Inject, Injector } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, NgControl } from '@angular/forms';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: InputComponent,
      multi: true
    }
  ],
  styleUrls: ['./input.component.scss']
})
export class InputComponent implements OnInit, ControlValueAccessor {
  @Input() type: string;
  @Input() label: string;
  value: string;
  ngControl: NgControl;

  constructor(
    private injector: Injector
  ) {}

  ngOnInit() {
    this.ngControl = this.injector.get(NgControl);
  }

  isInvalid() {
    return this.ngControl.dirty && !this.ngControl.valid;
  }

  getErrorKeys() {
    return Object.keys(this.ngControl.errors || {});
  }

  touchedFn() {}
  changeFn(v: string) {}

  writeValue(value: string) {
    this.value = value;
    this.changeFn(this.value);
  }

  registerOnChange(changeFn: any) {
    this.changeFn = changeFn;
  }

  registerOnTouched(touchedFn: any) {
    this.touchedFn = touchedFn;
  }
}
