import { Component, OnInit } from '@angular/core';
import { ModalService } from './modal.service';
import { ModalEventsService } from './modal-events.service';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {

  constructor(
    private modalEventsService: ModalEventsService
  ) { }

  ngOnInit() {
  }

  closeModal() {
    this.modalEventsService.closed$.next();
  }

  ngOnDestroy() {
    alert('modal content destro');
  }

}
