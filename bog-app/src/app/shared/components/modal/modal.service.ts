import { ApplicationRef, ComponentFactoryResolver, ComponentRef, Injectable, Injector, Type } from '@angular/core';
import { ForgotPasswordComponent } from '../../../forgot-password/forgot-password.component';
import { ModalComponent } from './modal.component';
import { ModalEventsService } from './modal-events.service';

@Injectable({
  providedIn: 'root'
})
export class ModalService {
  modalComponentRef: ComponentRef<ModalComponent>;

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private injector: Injector,
    private applicationRef: ApplicationRef,
    private modalEventsService: ModalEventsService
  ) { }

  open(component: Type<any>) {
    this.modalComponentRef = this.componentFactoryResolver
      .resolveComponentFactory(ModalComponent)
      .create(this.injector, this.getModalComponentContent(component));
    this.modalComponentRef.hostView.detectChanges();

    this.applicationRef.attachView(this.modalComponentRef.hostView);
    document.body.appendChild(this.modalComponentRef.location.nativeElement);

    this.modalEventsService.closed$.subscribe(() => this.close());
  }

  close() {
    this.applicationRef.detachView(this.modalComponentRef.hostView);
    this.modalComponentRef.hostView.destroy();
  }

  private getModalComponentContent(component: Type<any>) {
    const componentRef = this.componentFactoryResolver
      .resolveComponentFactory(component).create(this.injector);
    componentRef.hostView.detectChanges();
    return [
      [
        componentRef.location.nativeElement
      ]
    ];
  }
}
