import { Component, ContentChild, Input, OnInit, TemplateRef, ViewChild } from '@angular/core';

@Component({
  selector: 'app-nav-item',
  templateUrl: './nav-item.component.html',
  styleUrls: ['./nav-item.component.scss']
})
export class NavItemComponent implements OnInit {
  @Input() label: string;
  @Input() className = 'text-uppercase';
  @Input() textUppercase = true;

  @ContentChild(TemplateRef) labelTemplateRef: TemplateRef<any>;
  @ViewChild(TemplateRef) templateRef: TemplateRef<any>;

  constructor() { }

  ngOnInit() {
  }
}
