import { Component, ContentChildren, Input, OnInit, QueryList, ViewChild } from '@angular/core';
import { NavItemComponent } from './nav-item/nav-item.component';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {
  @ContentChildren(NavItemComponent) navItems: QueryList<NavItemComponent>;
  @ViewChild(NavItemComponent) viewNavItem;
  @Input() className: string;

  activeTabIndex = 0;

  constructor() { }

  ngOnInit() {
    console.log(this);
  }

  // ngAfterContentInit() {
  //   this.navItems.forEach((el, i) => {
  //     el.labelTemplateRef.index = i;
  //   });
  // }

  onTabClick($event: MouseEvent, i: number) {
    $event.preventDefault();
    this.activeTabIndex = i;
  }
}
