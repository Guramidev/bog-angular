import { Component, ElementRef, HostBinding, HostListener, OnInit }
  from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';

@Component({
  selector: 'app-ripple, [appRipple]',
  templateUrl: './ripple.component.html',
  styleUrls: ['./ripple.component.scss'],
  animations: [
    trigger('ripple', [
      state('initial', style({
        borderRadius: '100%',
        transform: 'scale(0)',
        opacity: 1
      })),
      state('active', style({
        borderRadius: 0,
        transform: 'scale(2.5)',
        opacity: 0
      })),
      transition('initial => active', animate(500))
    ])
  ]
})
export class RippleComponent implements OnInit {
  @HostBinding('style.position') position = 'relative';

  offsetLeft: string;
  offsetTop: string;
  scaleSize: number;
  rippleState = 'initial';

  constructor(
    private elementRef: ElementRef
  ) { }

  ngOnInit() {
  }

  @HostListener('click', ['$event'])
  onClick(e: MouseEvent) {
    e.preventDefault();
    this.calculateScaleSize();
    this.offsetLeft = (e.offsetX - this.scaleSize / 2) + 'px';
    this.offsetTop = (e.offsetY - this.scaleSize / 2) + 'px';
    this.rippleState = 'active';
  }

  onRippleAnimationDone() {
    this.rippleState = 'initial';
  }

  private calculateScaleSize() {
    this.scaleSize = Math.max(
      this.elementRef.nativeElement.offsetWidth,
      this.elementRef.nativeElement.offsetHeight
    );
  }
}
